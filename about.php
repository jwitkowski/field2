

<?php


/* Template Name:about */
?>



<?php


get_header(); ?>




	<div class="about">
		<div class="unit about-span-grid">

			
			<div class="about-text">
			<h1>We are a digtal marketing & design agency.</h1>
			<div class="two-of-five ">
				<p>We work with clients of all sizes, ranging from individuals to large corporations.</p>
				<p>Our formal education (each of us has an M.F.A) is in design and fine arts, and we believe that this sets us apart. It gives us an eye for core design concepts; e.g., color theory, typography, composition. Bla bla bla more text.Bla bla bla more text.Bla bla bla more text.Bla bla bla more text.</p>
			</div><!--/one-of-four-->
			<div class="two-of-five ">
				<p>Our extensive work experience, on the other hand, gives us a comprehensive understanding of a wide range of clients. We have worked with startups and large corporations, individual artists and national galleries, local institutions and firms overseas.</p>
				<p>We are WordPress experts, and use it on the majority of our web projects. But at our core, we are user experience fanatics. We value positive user experience as the most important factor in determining a project's success.</p>
			</div><!--/one-of-four-->
		</div><!--/about-text-->
		</div><!--/unit-->

		<div class="row about-content">
			<h2>Our Services</h2>
				<div class="one-of-three">
					<h3>Precision html/css cutup.</h3>
				</div>
				<div class="two-of-three">
					<p>We can take your prototype in any format--Photoshop, Fireworks, Illustrator, Indesign, PDF, even Quark--and convert it into a functioning site, built with clean, valid code.</p>
				</div>
			
				<div class="one-of-three">
					<h3>WordPress design and development. </h3>
				</div>
				<div class="two-of-three">
					<p>All our sites are built with WordPress as the underlying Content Management System (CMS)—meaning your site is easy to update by YOU, and that it can include the most powerful plugins available. WordPress has been called the most search engine friendly CMS available. We can also take your existing, static website and integrate WordPress into it.</p>
				</div>
				<div class="one-of-three">
					<h3>Graphical User Interface (GUI) design.  </h3>
				</div>
				<div class="two-of-three">
					<p>Field2 specializes in planning and prototyping websites and web applications with user-friendly interfaces, beautiful icons, and clear, to-the-point copy. You'll be rewarded with a site that welcomes new visitors and retains existing ones.</p>
				</div>
				<div class="one-of-three">
					<h3>Online advertising. </h3>
				</div>
				<div class="two-of-three">
					<p>We are experts at custom social media app development. If you have a marketing campaign that requires an online presence, we will make sure there is consistency in both the look/feel and the content.</p>
				</div>
				<div class="one-of-three">
					<h3>Training.</h3>
				</div>
				<div class="two-of-three">
					<p>With advanced degrees in fine art, and years of teaching courses in design and development, we can structure courses at any level and for any duration of time.</p>
				</div>
				<div style="clear:both;"></div>
				<br/><br/>
				
			<h2>Meet the team</h2>
			<div class="one-of-three team"><img src="images/jeanne.jpg"/><p>Jeanne Dunkle<br/><em>Creative Director, Owner</em></p></div>
			<div class="one-of-three team"> <img src="images/jen.jpg"/><p>Jen Witkowski<br/><em>Designer, Developer</em></p></div>
			<div class="one-of-three team"> <img src="images/ben.jpg"/><p>Ben Dunkle<br/><em>Design Consultant</em></p></div>
		</div><!--/row-->

					

				

	
		
	</div>
		


<?php get_footer(); ?>