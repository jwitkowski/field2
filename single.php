<?php $current="menu-item-15";
?>
<?php
/**
 * @package WordPress
 * @subpackage f2
 * @since f2 1.0
 Template Name: home
 */

include('header.php'); ?>

<div class="row blog-content">

 <div class="one-of-three blog-nav">
<h2 class="blue-bar">From the blog</h2>
<ul>
<?php

$args = array ( 'category_name' => 'blog' );
$custom_query = new WP_Query( $args );

if ( $custom_query->have_posts() ):
    while ( $custom_query->have_posts() ) :
        $custom_query->the_post();

        // Do stuff with the post content.
        echo '<li><a href="';
        the_permalink();
        echo '">';
        the_title();
        echo '</a></li>';

    endwhile;
else:
    // Insert any content or load a template for no posts found.
endif;

wp_reset_query();

?>
</ul>
</div>


     <!-- .col2 -->
<div class="two-of-three">
<?php if (have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>   
               <h2 class="blue-bar"><?php the_title(); ?></h2>
<em>Posted on <?php the_date(); ?> by  <?php the_author(); ?> </em> 

<?php the_content(); ?>
<?php comments_template(); ?> 
               <?php endwhile; ?>
     <?php endif; ?>
  </div><!-- .col2 -->
</div>

<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

<?php get_footer(); ?>