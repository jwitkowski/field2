<?php $current="nav_blog";
$title="Blog-under construction!";
?>
<?php
/**
 * @package WordPress
 * @subpackage f2
 * @since f2 1.0
 Template Name: blog
 */
include('header.php'); ?>
<div class="row blog-content">
	<div class="one-of-three">
 <?php query_posts('category_name=blog'); ?>
<?php if (have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>  
                 <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
Posted on <?php the_date(); ?> by  <?php the_author(); ?> 
               <?php endwhile; ?>
     <?php endif; ?>
 </div><!--/one-of-three-->
</div><!-- .blogdir -->
<?php get_footer(); ?>