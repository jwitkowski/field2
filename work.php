

<?php


/* Template Name:work */
?>



<?php


get_header(); ?>




	<div class="portfolio">
		<h2>Web</h2>
<?php $args = array( 'post_type' => 'Portfolio', 'portfolio_category' => 'web',  'orderby' => 'title',  'order' => 'ASC', 'posts_per_page' => 30 ); 
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<div class="unit one-of-four  port_contain" style="position:relative" >
			
                                        <?php the_content() ?>

				<div class="port_text" style="position:absolute; top:0px; left:0px;"><?php the_title(); ?></div>
	
				
				
				</div>
				
				
				<?php endwhile; ?>

				<h2>Identity</h2>
<?php $args = array( 'post_type' => 'Portfolio', 'portfolio_category' => 'logos',  'orderby' => 'title',  'order' => 'ASC', 'posts_per_page' => 30 ); 
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<div class="unit one-of-four  port_contain" style="position:relative" >
					<?php the_content() ?>
				

				<div class="port_text" style="position:absolute; top:0px; left:0px;"><?php the_title(); ?></div>
	
				
				
				</div>
				
				<?php endwhile; ?>


				<h2>Icons</h2>
<?php $args = array( 'post_type' => 'Portfolio', 'portfolio_category' => 'icons',  'orderby' => 'title',  'order' => 'ASC', 'posts_per_page' => 30 ); 
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<div class="unit one-of-four  port_contain" style="position:relative" >
					<?php the_content() ?>
				

				<div class="port_text" style="position:absolute; top:0px; left:0px;"><?php the_title(); ?></div>
	
				
				
				</div>
				
				<?php endwhile; ?>

					

				

	
		
	</div>
		


<?php get_footer(); ?>