<?php


if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150 );
	add_image_size( 'single-post-thumbnail',  150, 150, true ); 
	add_image_size( 'portfolio-thumbnail',  699, 663, true ); 
	add_image_size( 'product-pack-thumbnail',  113, 203, true ); 
  add_image_size( 'product-pack-large2',  187, 341, true ); 
  add_image_size( 'lip-image',  600, 600, true ); 
}

function my_excerpt_length($length) {
  		return 50;
}

add_filter('excerpt_length', 'my_excerpt_length');



function new_excerpt_more($more) {
       global $post;
	return '...<a href="'. get_permalink($post->ID) . '"><img src="images/btn_readmore.jpg" class="btn_right"/></a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function register_my_menus() {
  register_nav_menus(
    array('main' => __( 'main'), 'products' => __( 'products') )

  );
}
add_action( 'init', 'register_my_menus' );

function my_custom_post_portfolio() {
  $labels = array(
    'name'               => _x( 'Portfolios', 'post type general name' ),
    'singular_name'      => _x( 'Portfolio', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'item' ),
    'add_new_item'       => __( 'Add New Item' ),
    'edit_item'          => __( 'Edit Item' ),
    'new_item'           => __( 'New Item' ),
    'all_items'          => __( 'All Items' ),
    'view_item'          => __( 'View Items' ),
    'search_items'       => __( 'Search Items' ),
    'not_found'          => __( 'No items found' ),
    'not_found_in_trash' => __( 'No Items found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Portfolio'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our work and work specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'portfolio', $args ); 
}
add_action( 'init', 'my_custom_post_portfolio' );

function my_taxonomies_portfolio() {
  $labels = array(
    'name'              => _x( 'Portfolio Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Portfolio Categories' ),
    'all_items'         => __( 'All Portfolio Categories' ),
    'parent_item'       => __( 'Parent Portfolio Category' ),
    'parent_item_colon' => __( 'Parent Portfolio Category:' ),
    'edit_item'         => __( 'Edit Portfolio Category' ), 
    'update_item'       => __( 'Update Portfolio Category' ),
    'add_new_item'      => __( 'Add New Portfolio Category' ),
    'new_item_name'     => __( 'New Portfolio Category' ),
    'menu_name'         => __( 'Portfolio Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'portfolio_category', 'portfolio', $args );
}
add_action( 'init', 'my_taxonomies_portfolio', 0 );




?>