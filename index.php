<?php


get_header(); ?>

	<div class="flexslider">
			<ul class="slides">
		
			<li data-thumb="images/slide1_thumb.jpg"><img src="images/slideshow1_fpo.jpg"/>
				<h1>Expert WordPress designers & developers</h1>

			</li>
			<li data-thumb="images/slide1_thumb.jpg"><img src="images/slideshow1_fpo.jpg"/></li>
		</ul>
	</div>
	<div class="row">
		<div class="unit one-of-two" >
			<h2 class="blue-bar">We are Field 2 Design</h2>
			<p>Field 2 design is a web design and development company located in Buffalo, NY, specializing in SEO friendly, standards-compliant websites and graphics.</p>
<p>We work with clients of all sizes, ranging from individuals to large corporations.</p>
		</div><!--/one-of-two-->
		<div class="one-of-three services">
			<h3>Our Services Include...</h3>
			<ul>
				<li>Precision html/css cutup.</li>
				<li>Wordpress design and development. </li>
				<li>Graphical User Interface (GUI) design.</li>
				<li>Online advertising. </li>
				<li>Training.</li>
			</ul>
			<img src="images/icons_services.gif"/>
		</div>
	</div><!--/row-->
		


<?php get_footer(); ?>